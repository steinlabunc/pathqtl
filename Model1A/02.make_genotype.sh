#!/bin/bash
#SBATCH -p steinlab
#SBATCH -n 1
#SBATCH --mem=10g
#SBATCH -t 3:00:00

#Generate list of snps for specific chromosomes, and filter out snps with missing any of the subjects
module add plink;

for i in {1..22}
do

#eQTL

plink --bfile /proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Genotype_Hg38/${i}.dose.R2g03.QC.IDfixed \
      --freqx \
      --make-bed \
      --nonfounders \
      --keep ../progenitor.eqtl.donor.txt \
      --out  ../genotype/eQTL/chr${i}_Progenitor;


awk '$5 != 1 && $6 > 1 && $7 != 1 {print $2}' ../genotype/eQTL/chr${i}_Progenitor.frqx > ../genotype/eQTL/chr${i}_p_list.txt;
sed -i '1d' ../genotype/eQTL/chr${i}_p_list.txt;

plink --bfile /proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Genotype_Hg38/${i}.dose.R2g03.QC.IDfixed \
      --freqx \
      --make-bed \
      --nonfounders \
      --keep ../neuron.eqtl.donor.txt \
      --out ../genotype/eQTL/chr${i}_Neuron;


awk '$5 != 1 && $6 > 1 && $7 != 1 {print $2}' ../genotype/eQTL/chr${i}_Neuron.frqx > ../genotype/eQTL/chr${i}_n_list.txt;
sed -i '1d' ../genotype/eQTL/chr${i}_n_list.txt;

# caQTL

plink --bfile /proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Genotype_Hg38/${i}.dose.R2g03.QC.IDfixed \
      --freqx \
      --make-bed \
      --nonfounders \
      --keep ../progenitor.caqtl.donor.txt \
      --out  ../genotype/caQTL/chr${i}_Progenitor;


awk '$5 != 1 && $6 > 1 && $7 != 1 {print $2}' ../genotype/caQTL/chr${i}_Progenitor.frqx > ../genotype/caQTL/chr${i}_p_list.txt;
sed -i '1d' ../genotype/caQTL/chr${i}_p_list.txt;

plink --bfile /proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Genotype_Hg38/${i}.dose.R2g03.QC.IDfixed \
      --freqx \
      --make-bed \
      --nonfounders \
      --keep ../neuron.caqtl.donor.txt \
      --out ../genotype/caQTL/chr${i}_Neuron;


awk '$5 != 1 && $6 > 1 && $7 != 1 {print $2}' ../genotype/caQTL/chr${i}_Neuron.frqx > ../genotype/caQTL/chr${i}_n_list.txt;
sed -i '1d' ../genotype/caQTL/chr${i}_n_list.txt;



#eQTL

plink --bfile /proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Genotype_Hg38/${i}.dose.R2g03.QC.IDfixed \
      --keep ../progenitor.eqtl.donor.txt \
      --extract ../genotype/eQTL/chr${i}_p_list.txt \
      --make-bed \
      --recode12 \
      --output-missing-genotype 0 \
      --transpose \
      --out ../genotype/eQTL/chr${i}_P.uniq


plink --bfile /proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Genotype_Hg38/${i}.dose.R2g03.QC.IDfixed \
      --keep ../neuron.eqtl.donor.txt \
      --extract ../genotype/eQTL/chr${i}_n_list.txt \
      --make-bed \
      --recode12 \
      --output-missing-genotype 0 \
      --transpose \
      --out ../genotype/eQTL/chr${i}_N.uniq

# caQTL

plink --bfile /proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Genotype_Hg38/${i}.dose.R2g03.QC.IDfixed \
      --keep ../progenitor.caqtl.donor.txt \
      --extract ../genotype/caQTL/chr${i}_p_list.txt \
      --make-bed \
      --recode12 \
      --output-missing-genotype 0 \
      --transpose \
      --out ../genotype/caQTL/chr${i}_P.uniq


plink --bfile /proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Genotype_Hg38/${i}.dose.R2g03.QC.IDfixed \
      --keep ../neuron.caqtl.donor.txt \
      --extract ../genotype/caQTL/chr${i}_n_list.txt \
      --make-bed \
      --recode12 \
      --output-missing-genotype 0 \
      --transpose \
      --out ../genotype/caQTL/chr${i}_N.uniq


done
                                                    
#!/bin/bash
#SBATCH -n 1
#SBATCH -p general
#SBATCH --mem=4g
#SBATCH -t 6:00:00

#-------eQTL
# Progenitor
module add plink;
link="/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/genotype/eQTL/"
i=$1
file="/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/genotype/eQTL/truncated/progenestrun${i}.txt"

while read -r chr val1 val2 gene
  do
  plink -bfile ${link}/chr${chr}_P.uniq \
        --chr ${chr} --from-bp ${val1} \
        --out ${link}/truncated/progenitor/chr${chr}/P-chr${chr}-${gene} \
        --output-missing-genotype 0 --make-bed \
        --recode 12 --to-bp ${val2} --transpose


  done<$file

# Neuron
link="/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/genotype/eQTL/"
i=$1
file="/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/genotype/eQTL/truncated/neurogenestrun${i}.txt"

while read -r chr val1 val2 gene
  do
  plink -bfile ${link}/chr${chr}_N.uniq \
        --chr ${chr} --from-bp ${val1} \
        --out ${link}/truncated/neuron/chr${chr}/N-chr${chr}-${gene} \
        --output-missing-genotype 0 --make-bed \
        --recode 12 --to-bp ${val2} --transpose


  done<$file

#------caQTL

link="/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/genotype/caQTL/"
i=$1
file="/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/genotype/caQTL/truncated/chr${i}_chromatin_truncated.txt"

while read -r chr val1 val2 peak
  do
  plink -bfile ${link}/chr${chr}_N.uniq \
        --chr ${chr} --from-bp ${val1} \
        --out ${link}/truncated/neuron/chr${chr}/${peak} \
        --output-missing-genotype 0 --make-bed \
        --recode 12 --to-bp ${val2} --transpose

  plink -bfile ${link}/chr${chr}_P.uniq \
        --chr ${chr} --from-bp ${val1} \
        --out ${link}/truncated/progenitor/chr${chr}/${peak} \
        --output-missing-genotype 0 --make-bed \
        --recode 12 --to-bp ${val2} --transpose


  done<$file


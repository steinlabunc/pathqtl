#!/bin/bash
#SBATCH -p steinlab
#SBATCH -n 1
#SBATCH --mem=8g
#SBATCH -t 3:00:00

module add plink
module add emmax

i=$1
chr=$2


plink --bfile /proj/steinlab/projects/R00/atac-qtl/GenotypeData/PlinkData_original/QCMergedData/allmgergedGenoMafHweQCnew \
      --chr $chr --keep ../progenitor.eqtl.donor.sorted.txt \
      --make-bed --out snpseqtlprowo$i \
      --output-missing-genotype 0 --recode 12 --transpose

plink --bfile /proj/steinlab/projects/R00/atac-qtl/GenotypeData/PlinkData_original/QCMergedData/allmgergedGenoMafHweQCnew \
      --chr $chr --keep ../progenitor.caqtl.donor.sorted.txt \
      --make-bed --out snpscaqtlprowo$i \
      --output-missing-genotype 0 --recode 12 --transpose

plink --bfile /proj/steinlab/projects/R00/atac-qtl/GenotypeData/PlinkData_original/QCMergedData/allmgergedGenoMafHweQCnew \
      --chr $chr --keep ../neuron.eqtl.donor.sorted.txt \
      --make-bed --out snpseqtlneurowo$i \
      --output-missing-genotype 0 --recode 12 --transpose

plink --bfile /proj/steinlab/projects/R00/atac-qtl/GenotypeData/PlinkData_original/QCMergedData/allmgergedGenoMafHweQCnew \
      --chr $chr --keep ../neuron.caqtl.donor.sorted.txt \
      --make-bed --out snpscaqtlneurowo$i \
      --output-missing-genotype 0 --recode 12 --transpose


emmax-kin -v -h -d 10 snpseqtlprowo$i;
emmax-kin -v -h -d 10 snpscaqtlprowo$i;
emmax-kin -v -h -d 10 snpseqtlneurowo$i;
emmax-kin -v -h -d 10 snpscaqtlneurowo$i;

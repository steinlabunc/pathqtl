#!/bin/bash
#SBATCH -p general
#SBATCH -n 1
#SBATCH --mem=20g
#SBATCH -t 7:00:00


i=$1

#for j in {1..22}
#do
# awk '{print $1,$2,$3,FILENAME}' /pine/scr/n/i/nil1/trans-QTL/progenitor/chr${i}/P*chr${j}-*.ps > /pine/scr/n/i/nil1/progenitor_big_trans_chr${i}_${j}.txt
#done


cat /pine/scr/n/i/nil1/progenitor_big_trans_chr${i}_*QC_mappability.txt | awk '{print $1,$2,$3,$4,$5,$8,$9,$10,$11}' > /pine/scr/n/i/nil1/progenitor_big_trans_chr${i}.txt

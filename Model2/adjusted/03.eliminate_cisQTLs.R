options(stringAsFactors = F)

library(data.table)
gtf = fread("/proj/steinlab/projects/R00/eQTLanalysis/phenofiles/gene_transcript_v92.csv",data.table = F)
gtf = gtf[which(gtf$type == "gene"),]
end = gtf$end
idx = which(gtf$strand == "-")
gtf$start[idx] = gtf$end[idx]

type = commandArgs(trailingOnly=TRUE)[1]
k = commandArgs(trailingOnly=TRUE)[2]

ss = fread(paste0("/pine/scr/n/i/nil1/",type,"_big_trans_chr",k,"_",k,".txt"),data.table=F)
ss$gene = sapply(ss$V4, function(x) unlist(strsplit(x,"-",fixed="TRUE"))[4])
ss$gene = gsub(".ps","",ss$gene)
ss$tss = gtf$start[match(ss$gene,gtf$gene_id)]
ss$bp = as.numeric(sapply(ss$V1, function(x) unlist(strsplit(x,":",fixed="TRUE"))[2]))
ss$dist = abs(ss$bp - ss$tss)
ss = ss[which(ss$dist > 1e+6),]

write.table(ss[,1:4], file = paste0("/pine/scr/n/i/nil1/",type,"_big_trans_chr",k,"_",k,".txt"),
            quote = F, col.names = F, row.names=F)

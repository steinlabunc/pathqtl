options(stringAsFactors = F)

library(data.table)
type = commandArgs(trailingOnly=TRUE)[1]
k = commandArgs(trailingOnly=TRUE)[2]

library(data.table)
gtf = fread("/proj/steinlab/projects/R00/eQTLanalysis/phenofiles/gene_transcript_v92.csv",data.table = F)
gtf = gtf[which(gtf$type == "gene"),]

# Extract eGenes

sig_genes = read.table("/proj/steinlab/projects/R00/eQTLanalysis/multiple_test_correction/eigenMT/input/bulk/sig.genes", header=T)
p = max(as.numeric(sig_genes$pvalue))
sig_genes$biotype = gtf$gene_biotype[match(sig_genes$gene,gtf$gene_id)]
sig_genes = sig_genes[-grep("pseudogene",sig_genes$biotype),]



link = "/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/trans-eQTL/"

#sig_genes = read.table("/proj/steinlab/projects/R00/eQTLanalysis/multiple_test_correction/eigenMT/input/bulk/sig.genes", header=T)
assoc = fread(paste0("/proj/steinlab/projects/R00/eQTLanalysis/multiple_test_correction/eigenMT/input/bulk/qtls",k,".txt.gz"), data.table=F)
#assoc = assoc[which(assoc$gene %in% intersect(assoc$gene,sig_genes$gene)),]
#assoc = assoc[which(as.numeric(assoc$pvalue) <= p),]
assoc$biotype = gtf$gene_biotype[match(assoc$gene,gtf$gene_id)]
assoc = assoc[-grep("pseudogene",assoc$biotype),]
assoc = assoc[which(as.numeric(assoc$pvalue) <= p),]


sig = read.csv(paste0(link,"FDR25_MAF025_trans.",type,".big.bmediate.chr",k,"_updated_w_complete.csv"))
sig$id = paste0(sig$snp,"_",sig$cis_gene,"_",sig$trans_gene)
sig = sig[match(unique(sig$id),sig$id),]

link = "/pine/scr/n/i/nil1/"
ps_link = "/pine/scr/n/i/nil1/trans-QTL/bulk/chr"
dat = data.frame()

for (gene in unique(sig$trans_gene)) {
  trans_chr = unique(sig$trans_chr[which(sig$trans_gene == gene)])
  if (file.exists(paste0(ps_link,k,"/F-",trans_chr,"-",gene,".ps"))) {
    ps = read.table(paste0(ps_link,k,"/F-",trans_chr,"-",gene,".ps"))
    sub = sig[which(sig$trans_gene == gene),]
    sub$bulk_pval = ps$V3[match(sub$snp,ps$V1)]
    sub$bulk_beta = ps$V2[match(sub$snp,ps$V1)]
    dat = rbind(dat,sub)
  }
}

dat2 = data.frame()
for (snp in unique(dat$snp)) {
  sub = dat[which(dat$snp == snp),]
  ss1 = data.frame(sub,
                   bulk_cis_gene = assoc$gene[match(sub$snp,assoc$snp)])
  dat2 = rbind(dat2,ss1)
}

link = "/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/trans-eQTL/"
write.csv(dat2,file= paste0(link,"FDR25_MAF025_",type,".chr",k,"_trans_matched_bulk.csv"),
          quote = F, col.names=T, row.names = F)


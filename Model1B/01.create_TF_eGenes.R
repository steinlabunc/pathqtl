# Get TF-eQTLs to serach for motifs within chromatin accessible regions

# Data from http://humantfs.ccbr.utoronto.ca/download.php
tfcsv = read.csv("/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/data/tf_list.csv")
tfcsv = tfcsv[which(tfcsv$Is.TF. == "Yes"),]
tf_motif = tfcsv[which(tfcsv$TF.assessment == "Known motif"),]

link = "/proj/steinlab/projects/R00/eQTLanalysis/multiple_test_correction/eigenMT/input/cell/path_QTL/progenitor/eQTL/"
all_gene = data.frame()

for (i in 1:22) {
  sub = read.table(paste0(link,"chr",i,"_N"),header=T)
  all_gene = rbind(all_gene,sub)
}

all_gene = all_gene[which(all_gene$gene %in% intersect(all_gene$gene,tfcsv$Ensembl.ID)),]
all_gene$FDR = p.adjust(all_gene$BF,method = "fdr", n = nrow(all_gene))
all_gene = all_gene[which(all_gene$FDR < 0.05),]

write.table(all_gene,
            file="/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/data/progenitor_tf_eGenes.txt",
            quote=F,col.names=T,row.names=F)
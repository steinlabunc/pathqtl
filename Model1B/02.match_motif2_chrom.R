# Find TF eGene motifs within chromatin accessbile regions throughout the genome

options(stringAsFactors = F)

# Filter chromatin regions with motifs

chr = commandArgs(trailingOnly=TRUE)[1]
i1 = commandArgs(trailingOnly=TRUE)[2]
i2 = commandArgs(trailingOnly=TRUE)[3]

# Get TFs PWM list from https://pubmed.ncbi.nlm.nih.gov/29425488/
# http://humantfs.ccbr.utoronto.ca/download.php

dt = read.csv("/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/trans-eQTL/data/pwm_lambert/PWMs/Human_TF_MotifList_v_1.01.csv")
gene1 = read.table("/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/data/progenitor_tf_eGenes.txt",header = T)
gene2 = read.table("/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/data/neuron_tf_eGenes.txt",header = T)
genes = unique(c(gene1$gene,gene2$gene))
dt = dt[which(dt$Ensembl.ID %in% intersect(dt$Ensembl.ID,genes)),]

options(stringsAsFactors=F)
outdir = "/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/phenotype/caQTL/"

library(DESeq2);
library(limma);
library(statmod);
library(TFBSTools)
library(JASPAR2020)
library(Biostrings)
library(BSgenome.Hsapiens.UCSC.hg38)

load("/proj/steinlab/projects/R00/ATACpreprocess/DifferentChromAccess/CSAW/Invitro/DESeq2/CSAW_100L_dds.Rdata");
dds <- DESeqDataSet(dds, design = ~1)
## keep phenotype files as the same order as genotype files
Nfam=read.table("/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/data/neuron.caqtl.donor.sorted.txt");
NIDs=paste0(Nfam$V1,"_",Nfam$V2);
Pfam=read.table("/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/data/progenitor.caqtl.donor.sorted.txt");
PIDs=paste0(Pfam$V1,"_",Pfam$V2);

## load peak coodinates
peaks=read.csv("/proj/steinlab/projects/R00/ATACpreprocess/DifferentChromAccess/CSAW/Invitro/Limma/ATAC_NeuronProgenitor_DifferentialPeaks.csv");
peaks$name = paste0(peaks$Chr,"_",peaks$peakstart,"_",peaks$peakend)
rownames(dds) = peaks$name

# Subset for cell type
colData(dds)$comb = paste0(dds$DonorID,"_",dds$DNAID)

dds.diff <- dds[, which(colData(dds)$CellType == "Neuron")]
dds.prolif <- dds[, which(colData(dds)$CellType == "Progenitor")]

dds.prolif1 <- dds.prolif[,match(PIDs, dds.prolif$comb)]
dds.diff1 <- dds.diff[,match(NIDs, dds.diff$comb)]

assay <- rbind(t(assay(dds.prolif1)),t(assay(dds.diff1)))
coldata <- rbind(colData(dds.prolif1),colData(dds.diff1))

dds <- SummarizedExperiment(assays = data.matrix(t(assay)), colData=coldata)
dds <- DESeqDataSet(dds, design = ~1)


vsd <- vst(dds)
peaks = rownames(vsd)[grep(paste0("chr",chr,"_"), rownames(vsd))]
peaks = peaks[i1:i2]
bp1 = as.numeric(sapply(peaks, function(x) unlist(strsplit(x,"_",fixed="TRUE"))[2]))
bp2 = as.numeric(sapply(peaks, function(x) unlist(strsplit(x,"_",fixed="TRUE"))[3]))



chrom_motif = data.frame()
datadir = "/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/trans-eQTL/data/pwm_lambert/PWMs/"
for (j in 1:nrow(dt)) {
     if (file.exists(paste0(datadir,dt$CIS.BP.ID[j],".txt"))) {
         pwm = read.table(paste0(datadir,dt$CIS.BP.ID[j],".txt"),header=T)
         if (nrow(pwm) >= 1) {
         pwm1 <- PWMatrix(ID=dt$Ensembl.ID[j], name=dt$CIS.BP.ID[j],
                         matrixClass=dt$Motif.type[j],strand="*",
                         bg=c(A=0.25, C=0.25, G=0.25, T=0.25),
                         profileMatrix=t(pwm[,-1]))

         pwmList1 <- PWMatrixList(pwm1,use.names=TRUE)
         chrom <- DNAStringSet(as.character(Biostrings::getSeq(BSgenome.Hsapiens.UCSC.hg38,
                                                               paste0("chr",chr),
                                                          start = bp1,
                                                          end = bp2)))
         names(chrom) = peaks
         sitesetList <- searchSeq(pwmList1, chrom, seqname="",
                                  min.score="80%", strand="*")

         dat = as(sitesetList, "data.frame")
         if (nrow(dt) >= 1) {
           chrom_motif = rbind(chrom_motif,dat)
         }
       }
     }

}

chrom_motif$id = paste0(chrom_motif$seqnames,"_",
                        chrom_motif$TF,"_",
                        chrom_motif$strand)

chrom_motif = chrom_motif[match(unique(chrom_motif$id),
                                chrom_motif$id),]

write.table(chrom_motif,
            file = paste0("/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/bmediate/data/motif_matching_chromatin_TF_pairs_chr",chr,"_",i1,"_",i2,".txt"),
            quote = F, col.names= T,row.names=F)
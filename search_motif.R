# Motif serach for figure 5, SLC26A7 gene locus

setwd("/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/")

options(stringsAsFactors = F)
library(data.table)
library(Biostrings)
library(BSgenome.Hsapiens.UCSC.hg38)
library(TFBSTools)
library(JASPAR2020)



# chr8:91180477:GT:G
start = 91180477 - 50
end = 91180477 + 50
chr = 8
seq1 = as.character(Biostrings::getSeq(BSgenome.Hsapiens.UCSC.hg38, paste0("chr",chr),
                                       start, end))
seq2 = unlist(strsplit(seq1,""))
seq2 = paste0(seq2[-52],collapse = "")
seq_gt <- DNAString(seq1)
seq_g <- DNAString(seq2)

pwms = list.files(path = "/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/trans-eQTL/data/",
                  pattern = glob2rx("*.jaspar"))

dat = data.frame()
for (p in pwms) {
    pfm <- getMatrixByID(JASPAR2020, ID=p)
    pwm1 <- toPWM(pfm)

    pwmList <- PWMatrixList(pwm1,use.names=TRUE)
    sitesetList1 <- searchSeq(pwmList, seq_gt, seqname="seq1",
                              min.score = 0)
    dt1 = as(sitesetList1, "data.frame")
    sitesetList1 <- searchSeq(pwmList, seq_g, seqname="seq2",
                              min.score = 0)
    dt2 = as(sitesetList1, "data.frame")
    dat = rbind(dat,dt1,dt2)
}

pfm <- getMatrixByID(JASPAR2020, ID="MA1566.1")
pwm1 <- toPWM(pfm)
pwmList <- PWMatrixList(pwm1,use.names=TRUE)
sitesetList1 <- searchSeq(pwmList, seq_g, seqname="seq2",
                          min.score = 0.9)
pvalues(sitesetList1, type="TFMPvalue")
dist = data.frame()

for (tf in unique(dat$TF)) {
  sub = dat[which(dat$TF == tf),]
  t = t.test(sub$relScore ~ as.factor(sub$seqnames), paired = FALSE)
  s= summary(lm(sub$relScore ~ as.factor(sub$seqnames)))
  dt = data.frame(TF = tf,pval = t$p.value,beta = s$coefficients[2,1])
  dist = rbind(dist,dt)
}
small_dat = dat[which(dat$start <= 50 & dat$end >= 50),]

dist = data.frame()

for (tf in unique(small_dat$TF)) {
  sub = small_dat[which(small_dat$TF == tf),]
  t = t.test(sub$relScore ~ as.factor(sub$seqnames), paired = FALSE)
  s= summary(lm(sub$relScore ~ as.factor(sub$seqnames)))
  dt = data.frame(TF = tf,pval = t$p.value,beta = s$coefficients[2,1])
  dist = rbind(dist,dt)
}

small_dat = dat[which(dat$start <= 50 & dat$end >= 50),]

dist = data.frame()

for (tf in unique(small_dat$TF)) {
  sub1 = small_dat[which(small_dat$TF == tf & small_dat$seqnames == "seq2"),]
  sub1 = sub1[which(sub1$relScore >= 0.85),]
  if (nrow(sub1) >= 1) {
      ord = paste0(sub1$start,"_",sub1$end,"_",sub1$strand)
      sub2 = small_dat[which(small_dat$TF == tf & small_dat$seqnames == "seq1"),]
      sub2$comb = paste0(sub2$start,"_",sub2$end,"_",sub2$strand)
      sub2 = sub2[match(ord,sub2$comb),]

      vec = sub1$relScore - sub2$relScore
      dt = data.frame(TF = tf,dif = vec,order = ord)
      dist = rbind(dist,dt)
  }
}

write.csv(dist,file="/pine/scr/n/i/nil1/backup/dist_chr8:91180271:A:G.csv",quote=F,col.names=T,row.names=F)
write.csv(dat,file="/pine/scr/n/i/nil1/backup/all_tf_motif_chr8:91180271:A:G.csv",quote=F,col.names=T,row.names=F)


# chr8:91180955:A:G
start = 91180271 - 50
end = 91180271 + 50
chr = 8
seq1 = as.character(Biostrings::getSeq(BSgenome.Hsapiens.UCSC.hg38, paste0("chr",chr),
                                       start, end))
seq2 = unlist(strsplit(seq1,""))
seq2[51] = "G"
seq2 = paste0(seq2,collapse = "")
seq_gt <- DNAString(seq1)
seq_g <- DNAString(seq2)

pwms = list.files(path = "/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/trans-eQTL/data/",
                  pattern = glob2rx("*.jaspar"))

dat = data.frame()

for (p in pwms) {
  pfm <- getMatrixByID(JASPAR2020, ID=p)
  pwm1 <- toPWM(pfm)

  pwmList <- PWMatrixList(pwm1,use.names=TRUE)
  sitesetList1 <- searchSeq(pwmList, seq_gt, seqname="seq1",
                            min.score = 0)
  dt1 = as(sitesetList1, "data.frame")
  sitesetList1 <- searchSeq(pwmList, seq_g, seqname="seq2",
                            min.score = 0)
  dt2 = as(sitesetList1, "data.frame")
  dat = rbind(dat,dt1,dt2)
}

dist = data.frame()

for (tf in unique(dat$TF)) {
  sub = dat[which(dat$TF == tf),]
  t = t.test(sub$relScore ~ as.factor(sub$seqnames), paired = FALSE)
  s= summary(lm(sub$relScore ~ as.factor(sub$seqnames)))
  dt = data.frame(TF = tf,pval = t$p.value,beta = s$coefficients[2,1])
  dist = rbind(dist,dt)
}


small_dat = dat[which(dat$start <= 50 & dat$end >= 50),]

dist = data.frame()

for (tf in unique(small_dat$TF)) {
  sub = small_dat[which(small_dat$TF == tf),]
  t = t.test(sub$relScore ~ as.factor(sub$seqnames), paired = FALSE)
  s= summary(lm(sub$relScore ~ as.factor(sub$seqnames)))
  dt = data.frame(TF = tf,pval = t$p.value,beta = s$coefficients[2,1])
  dist = rbind(dist,dt)
}

small_dat = dat[which(dat$start <= 50 & dat$end >= 50),]

dist = data.frame()

for (tf in unique(small_dat$TF)) {
  sub1 = small_dat[which(small_dat$TF == tf & small_dat$seqnames == "seq2"),]
  sub1 = sub1[which(sub1$relScore >= 0.85),]
  if (nrow(sub1) >= 1) {
      ord = paste0(sub1$start,"_",sub1$end,"_",sub1$strand)
      sub2 = small_dat[which(small_dat$TF == tf & small_dat$seqnames == "seq1"),]
      sub2$comb = paste0(sub2$start,"_",sub2$end,"_",sub2$strand)
      sub2 = sub2[match(ord,sub2$comb),]

      vec = sub1$relScore - sub2$relScore
      dt = data.frame(TF = tf,dif = vec,order = ord)
      dist = rbind(dist,dt)
  }
}


small_dat = dat[which(dat$start <= 50 & dat$end >= 50),]

dist = data.frame()

for (tf in unique(small_dat$TF)) {
  sub = small_dat[which(small_dat$TF == tf),]
  vec1 = sub$relScore[which(sub$seqnames == "seq1")]
  vec2 = sub$relScore[which(sub$seqnames == "seq2")]
  vec = as.vector(outer(vec1,vec2,"-"))
  dt = data.frame(TF = tf,dif = vec,order = 1:length(vec))
  dist = rbind(dist,dt)
}

write.csv(dist,file="/pine/scr/n/i/nil1/backup/dist_chr8:91180271:A:G.csv",quote=F,col.names=T,row.names=F)
write.csv(dat,file="/pine/scr/n/i/nil1/backup/all_tf_motif_chr8:91180955:A:G.csv",quote=F,col.names=T,row.names=F)
dat = read.csv("/pine/scr/n/i/nil1/backup/all_tf_motif_chr8:91180477:GT:G.csv")
dat$gene = gtf$gene_id[match(dat$TF,gtf$gene_name)]
dif = read.csv("/proj/steinlab/projects/R00/eQTLanalysis/Supplementary_tables/Table_S1.csv")
dif = dif[which(dif$logFC < 0),]
dat$pval = dif$adj.P.Val[match(dat$gene,dif$gene)]

small_dat = dat[which(dat$start <= 50 & dat$end >= 50),]

dist = data.frame()

for (tf in unique(small_dat$TF)) {
  sub1 = small_dat[which(small_dat$TF == tf & small_dat$seqnames == "seq2"),]
  sub1 = sub1[which(sub1$relScore >= 0.85),]
  if (nrow(sub1) >= 1) {
    ord = paste0(sub1$start,"_",sub1$end,"_",sub1$strand)
    sub2 = small_dat[which(small_dat$TF == tf & small_dat$seqnames == "seq1"),]
    sub2$comb = paste0(sub2$start,"_",sub2$end,"_",sub2$strand)
    sub2 = sub2[match(ord,sub2$comb),]

    vec = sub1$relScore - sub2$relScore
    dt = data.frame(TF = tf,dif = vec,order = ord)
    dist = rbind(dist,dt)
  }
}

# chr8:91180271:A:G
start = 91180271 - 50
end = 91180271 + 50
chr = 8
seq1 = as.character(Biostrings::getSeq(BSgenome.Hsapiens.UCSC.hg38, paste0("chr",chr),
                                       start, end))
seq2 = unlist(strsplit(seq1,""))
seq2[51] = "G"
seq2 = paste0(seq2,collapse = "")
seq_gt <- DNAString(seq1)
seq_g <- DNAString(seq2)

pwms = list.files(path = "/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/trans-eQTL/data/",
                  pattern = glob2rx("*.jaspar"))

dat = data.frame()

for (p in pwms) {
  pfm <- getMatrixByID(JASPAR2020, ID=p)
  pwm1 <- toPWM(pfm)

  pwmList <- PWMatrixList(pwm1,use.names=TRUE)
  sitesetList1 <- searchSeq(pwmList, seq_gt, seqname="seq1",
                            min.score = 0)
  dt1 = as(sitesetList1, "data.frame")
  sitesetList1 <- searchSeq(pwmList, seq_g, seqname="seq2",
                            min.score = 0)
  dt2 = as(sitesetList1, "data.frame")
  dat = rbind(dat,dt1,dt2)
}

dist = data.frame()

for (tf in unique(dat$TF)) {
  sub = dat[which(dat$TF == tf),]
  t = t.test(sub$relScore ~ as.factor(sub$seqnames), paired = FALSE)
  s= summary(lm(sub$relScore ~ as.factor(sub$seqnames)))
  dt = data.frame(TF = tf,pval = t$p.value,beta = s$coefficients[2,1])
  dist = rbind(dist,dt)
}


small_dat = dat[which(dat$start <= 50 & dat$end >= 50),]

dist = data.frame()

for (tf in unique(small_dat$TF)) {
  sub = small_dat[which(small_dat$TF == tf),]
  t = t.test(sub$relScore ~ as.factor(sub$seqnames), paired = FALSE)
  s= summary(lm(sub$relScore ~ as.factor(sub$seqnames)))
  dt = data.frame(TF = tf,pval = t$p.value,beta = s$coefficients[2,1])
  dist = rbind(dist,dt)
}

small_dat = dat[which(dat$start <= 50 & dat$end >= 50),]

dist = data.frame()

for (tf in unique(small_dat$TF)) {
  sub = small_dat[which(small_dat$TF == tf),]
  vec1 = sub$relScore[which(sub$seqnames == "seq1")]
  vec2 = sub$relScore[which(sub$seqnames == "seq2")]
  vec = as.vector(outer(vec1,vec2,"-"))
  dt = data.frame(TF = tf,dif = vec,order = 1:length(vec))
  dist = rbind(dist,dt)
}

write.csv(dist,file="/pine/scr/n/i/nil1/backup/dist_chr8:91180271:A:G.csv",quote=F,col.names=T,row.names=F)
write.csv(dat,file="/pine/scr/n/i/nil1/backup/all_tf_motif_chr8:91180271:A:G.csv",quote=F,col.names=T,row.names=F)



# Plot the TF motiff logo

require(ggplot2)
require(ggseqlogo)

pfm <- getMatrixByID(JASPAR2020, ID="MA1566.1")
ggseqlogo(as.matrix(pfm))

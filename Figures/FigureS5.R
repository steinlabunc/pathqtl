
#----a

options(stringAsFactors = F)
library(data.table)

link = "/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/bmediate/icc/"

icc_gene_p = data.frame()
for (i in 1:22) {
  dt = read.table(paste0(link,"progenitor_chr",i,"_icc_gene.txt"),header = T)
  icc_gene_p = rbind(icc_gene_p,dt)
}

icc_gene_n = data.frame()
for (i in 1:22) {
  dt = read.table(paste0(link,"neuron_chr",i,"_icc_gene.txt"),header = T)
  icc_gene_n = rbind(icc_gene_n,dt)
}

icc_peak_p = data.frame()
for (i in 1:22) {
  dt = read.table(paste0(link,"progenitor_chr",i,"_icc_peak.txt"),header = T)
  icc_peak_p = rbind(icc_peak_p,dt)
}

icc_peak_n = data.frame()
for (i in 1:22) {
  dt = read.table(paste0(link,"neuron_chr",i,"_icc_peak.txt"),header = T)
  icc_peak_n = rbind(icc_peak_n,dt)
}


capro = data.frame(icc = icc_peak_p$icc,
                   type = "progenitor",
                   molecule = "ATAC")
caneuro = data.frame(icc = icc_peak_n$icc,
                     type = "neuron",
                     molecule = "ATAC")
epro = data.frame(icc = icc_gene_p$icc,
                  type = "progenitor",
                  molecule = "RNA")
eneuro = data.frame(icc = icc_gene_n$icc,
                    type = "neuron",
                    molecule = "RNA")


trop = rbind(capro,epro,caneuro,eneuro)
trop$comb= paste0(trop$type,"_",trop$molecule)

library(ggplot2)
ggplot(trop,aes(factor(comb),icc)) + 
  geom_boxplot() 

prop = trop[which(trop$type == "progenitor"),]
nrop = trop[which(trop$type == "neuron"),]

A = t.test(prop$icc ~ as.factor(prop$molecule),data = prop)
2* pt(A$statistic,  df = A$parameter, lower.tail=FALSE)


#----b

set.seed(152)

sds <- seq(0,3,0.003)
s = 1
cat("running row #",s,"\n")

##############################################
PVE1 = 0.3 # x on m
PVE2 = 0.6 # m on y

iccs = c()
hers = c()
ys = c()
ms = c()
m <- x + rnorm(100,0,sqrt(1/PVE1- 1))
mscale <- m/sd(m)

y1 <- mscale + rnorm(100,0,sd=sqrt(1/PVE2 - 1))
#y = y1/sd(y1)
y=y1

par(mfrow = c(1,2))

  res1 = data.frame()
  res2 = data.frame()
  res3 = data.frame()
  res4 = data.frame()
  res5 = data.frame()
  res6 = data.frame()
  res7 = data.frame()
  res8 = data.frame()
  y_l = list()
  m_l = list()
  x_l = list()
  
  for (sd in sds) {
    
    e = rnorm(100,0,sd)
    j = which(sds == sd)
    
    m1 <- mscale + e
    #y1 <- y + e
    
    y=y
    #y=y1
    M=cbind(m1,m1)
    #M=cbind(mscale,mscale)
    X=as.matrix(x)
    names(y) = 1:100
    rownames(M) = 1:100
    colnames(M) = c("M1","M2")
    rownames(X) = 1:100
    colnames(X) = "SNP"
    
    g = cbind(mscale,m1)
    #g = cbind(y,y1)
    dt = data.frame(m = c(mscale,m1),id = rep(1:length(m1),2))
    #dt = data.frame(m = c(y1,y),id = rep(1:length(y1),2))
    Gm = apply(g,1,mean)
    l1 = lm(y ~ x)
    l2 = lm(m1 ~ x)
    #l2 = lm(mscale ~ x)
    l3 = lm(y ~ m1)
    #l3 = lm(y1 ~ mscale)
    m_fitted <- x*(summary(l2)$coefficients[2,1])
    #y_fitted <- m1*(summary(l3)$coefficients[2,1])
    y_fitted <- mscale*(summary(l3)$coefficients[2,1])
    
    l_med <- lm(y_fitted ~ m_fitted)
    m_her <- summary(l3)$r.square*summary(l2)$r.square
    
    y_l[[j]] = y
    m_l[[j]] = M[,1]
    x_l[[j]] = X
    
    res2 = rbind(res2, data.frame(a=summary(l2)$r.squared,
                                  b=summary(l3)$r.squared,
                                  c=summary(l1)$r.squared))
    
    res3 = rbind(res3, data.frame(beta = summary(l2)$r.squared))
    res4 = rbind(res4, data.frame(beta = summary(l3)$r.squared))
    res5 = rbind(res5, data.frame(beta = m_her))
    res8 = rbind(res8, data.frame(beta = ICC::ICCest(id,m,data=dt)$ICC))
    res9 = rbind(res4, data.frame(beta = summary(l1)$r.squared))
  }
  
  for (i in 1:nrow(res2)) {
    
    # format inputs
    y_ll = y_l[[i]]
    m_ll = m_l[[i]]
    x_ll = x_l[[i]]
    
    Mm=cbind(m_ll,m_ll)
    Xx=as.matrix(x_ll)
    names(y_ll) = 1:100
    rownames(Mm) = 1:100
    colnames(Mm) = c("M1","M2")
    rownames(Xx) = 1:100
    colnames(Xx) = "SNP"
    
    # Each element of (a, b, c) represents one of the relationships
    # being evaluated for mediation, specifically the ratio of signal to noise.
    # a is the effect of X on M, b is the effect of M on y, and c is the effect of X on y.
    
    # convert PVEs to odd ratio
    
    a1 = 1
    b1 = 1
    c1 = 1
    med = bmediatR(
      y = y_ll,
      M = Mm,
      X = Xx,
      Z = NULL,
      Z_y = NULL,
      Z_M = NULL,
      w = NULL,
      w_y = NULL,
      w_M = NULL,
      kappa = c(0.001, 0.001),
      lambda = c(0.001, 0.001),
      tau_sq_mu = c(1000, 1000),
      tau_sq_Z = c(1000, 1000),
      phi_sq = c(a1, b1, c1),
      ln_prior_c = "reactive",
      options_X = list(sum_to_zero = FALSE, center = FALSE, scale = FALSE),
      align_data = TRUE,
      verbose = TRUE
    )
    
    t1 = med$ln_post_c[,match(c("1,1,0", "1,1,1", "1,0,1", "1,*,1", "0,*,1"),colnames(med$ln_post_c))]
    #t1 = cbind(t1,(1-rowSums(t1)))
    colnames(t1) = c("complete med","partial med","co-local",
                     "partial med (react)","complete med (react)")
    
    
    res1 = rbind(res1, data.frame(beta = sds[i],t(t1[1,])))
    
  }
  
  res1$other.non.med = 1 - (rowSums(exp(res1[,-1])))
  res3$other.non.med = 1 - (rowSums(exp(res3[,-1])))
  res4$other.non.med = 1 - (rowSums(exp(res4[,-1])))
  res5$other.non.med = 1 - (rowSums(exp(res5[,-1])))
  res7$other.non.med = 1 - (rowSums(exp(res7[,-1])))
  res8$other.non.med = 1 - (rowSums(exp(res8[,-1])))
  
  #--ICC
  
  dticc = data.frame(icc = res8$beta,
                     comp_med = res1$complete.med,
                     par_med = res1$partial.med,
                     comp_med_reac = res1$complete.med..react.,
                     par_med_reac = res1$partial.med..react.,
                     coloc = res1$co.local,
                     non_med = res1$other.non.med)
  
  dticc = dticc[order(dticc$icc),]
  dticc[,2:6] = exp(dticc[,2:6])
  dticc$caus = dticc$comp_med + dticc$par_med
  dticc$reac = dticc$comp_med_reac + dticc$par_med_reac
  
  dat = rbind(data.frame(val = dticc$icc,
                         mod = dticc$caus,
                         group = "causal"),
              data.frame(val = dticc$icc,
                         mod = dticc$coloc,
                         group = "co-loc"),
              data.frame(val = dticc$icc,
                         mod = dticc$reac,
                         group = "reactive"),
              data.frame(val = dticc$icc,
                         mod = dticc$non_med,
                         group = "non_med"))
  
  
  ggplot(dat, aes(x=val, y=mod, color=group)) +
    geom_point() +
    scale_color_manual(values=c('blue','black','yellow','red')) +
    stat_smooth()
  
  # Mediated heritability
  
  dticc = data.frame(icc = res5$beta,
                     comp_med = res1$complete.med,
                     par_med = res1$partial.med,
                     comp_med_reac = res1$complete.med..react.,
                     par_med_reac = res1$partial.med..react.,
                     coloc = res1$co.local,
                     non_med = res1$other.non.med)
  
  dticc = dticc[order(dticc$icc),]
  dticc[,2:6] = exp(dticc[,2:6])
  dticc$caus = dticc$comp_med + dticc$par_med
  dticc$reac = dticc$comp_med_reac + dticc$par_med_reac

dat = rbind(data.frame(val = dticc$icc,
                       mod = dticc$caus,
                       group = "causal"),
            data.frame(val = dticc$icc,
                       mod = dticc$coloc,
                       group = "co-loc"),
            data.frame(val = dticc$icc,
                       mod = dticc$reac,
                       group = "reactive"),
            data.frame(val = dticc$icc,
                       mod = dticc$non_med,
                       group = "non_med"))


ggplot(dat, aes(x=val, y=mod, color=group)) +
  geom_point() +
  scale_color_manual(values=c('blue','black','yellow','red')) +
  stat_smooth()


# Heritability of M'

dticc = data.frame(icc = res3$beta,
                   comp_med = res1$complete.med,
                   par_med = res1$partial.med,
                   comp_med_reac = res1$complete.med..react.,
                   par_med_reac = res1$partial.med..react.,
                   coloc = res1$co.local,
                   non_med = res1$other.non.med)

dticc = dticc[order(dticc$icc),]
dticc[,2:6] = exp(dticc[,2:6])
dticc$caus = dticc$comp_med + dticc$par_med
dticc$reac = dticc$comp_med_reac + dticc$par_med_reac

dat = rbind(data.frame(val = dticc$icc,
                       mod = dticc$caus,
                       group = "causal"),
            data.frame(val = dticc$icc,
                       mod = dticc$coloc,
                       group = "co-loc"),
            data.frame(val = dticc$icc,
                       mod = dticc$reac,
                       group = "reactive"),
            data.frame(val = dticc$icc,
                       mod = dticc$non_med,
                       group = "non_med"))


ggplot(dat, aes(x=val, y=mod, color=group)) +
  geom_point() +
  scale_color_manual(values=c('blue','black','yellow','red')) +
  stat_smooth()



#----c


set.seed(152)
sds <- seq(0,3,0.03)

x <- rnorm(100)
s = 1
cat("running row #",s,"\n")

##############################################
PVE1 = 0.3 # x on m
PVE2 = 0.8 # m on y

iccs = c()
hers = c()
ys = c()
ms = c()

m <- x + rnorm(100,0,sqrt(1/PVE1- 1))
mscale <- m/sd(m)

y1 <- mscale + rnorm(100,0,sd=sqrt(1/PVE2 - 1))
#y = y1/sd(y1)
y=y1

par(mfrow = c(1,2))
for (mm in 1:10) {
  res1 = data.frame()
  res2 = data.frame()
  res3 = data.frame()
  res4 = data.frame()
  res5 = data.frame()
  res6 = data.frame()
  res7 = data.frame()
  res8 = data.frame()
  y_l = list()
  m_l = list()
  x_l = list()
  
  for (sd in sds) {
    
    e = rnorm(100,0,sd)
    j = which(sds == sd)
    
    m1 <- mscale + e
    #y1 <- y + e
    
    y=y
    #y=y1
    M=cbind(m1,m1)
    #M=cbind(mscale,mscale)
    X=as.matrix(x)
    names(y) = 1:100
    rownames(M) = 1:100
    colnames(M) = c("M1","M2")
    rownames(X) = 1:100
    colnames(X) = "SNP"
    
    g = cbind(mscale,m1)
    #g = cbind(y,y1)
    dt = data.frame(m = c(mscale,m1),id = rep(1:length(m1),2))
    #dt = data.frame(m = c(y1,y),id = rep(1:length(y1),2))
    Gm = apply(g,1,mean)
    l1 = lm(y ~ x)
    l2 = lm(m1 ~ x)
    #l2 = lm(mscale ~ x)
    l3 = lm(y ~ m1)
    #l3 = lm(y1 ~ mscale)
    m_fitted <- x*(summary(l2)$coefficients[2,1])
    #y_fitted <- m1*(summary(l3)$coefficients[2,1])
    y_fitted <- mscale*(summary(l3)$coefficients[2,1])
    
    l_med <- lm(y_fitted ~ m_fitted)
    m_her <- summary(l3)$r.square*summary(l2)$r.square
    
    y_l[[j]] = y
    m_l[[j]] = M[,1]
    x_l[[j]] = X
    
    res2 = rbind(res2, data.frame(a=summary(l2)$r.squared,
                                  b=summary(l3)$r.squared,
                                  c=summary(l1)$r.squared))
    
    res3 = rbind(res3, data.frame(beta = summary(l2)$r.squared))
    res4 = rbind(res4, data.frame(beta = summary(l3)$r.squared))
    res5 = rbind(res5, data.frame(beta = m_her))
    res8 = rbind(res8, data.frame(beta = ICC::ICCest(id,m,data=dt)$ICC))
    res9 = rbind(res4, data.frame(beta = summary(l1)$r.squared))
  }
  
  for (i in 1:nrow(res2)) {
    
    # format inputs
    y_ll = y_l[[i]]
    m_ll = m_l[[i]]
    x_ll = x_l[[i]]
    
    Mm=cbind(m_ll,m_ll)
    Xx=as.matrix(x_ll)
    names(y_ll) = 1:100
    rownames(Mm) = 1:100
    colnames(Mm) = c("M1","M2")
    rownames(Xx) = 1:100
    colnames(Xx) = "SNP"
    
    # Each element of (a, b, c) represents one of the relationships
    # being evaluated for mediation, specifically the ratio of signal to noise.
    # a is the effect of X on M, b is the effect of M on y, and c is the effect of X on y.
    
    # convert PVEs to odd ratio
    
    a1 = 1
    b1 = 1
    c1 = 1
    med = bmediatR(
      y = y_ll,
      M = Mm,
      X = Xx,
      Z = NULL,
      Z_y = NULL,
      Z_M = NULL,
      w = NULL,
      w_y = NULL,
      w_M = NULL,
      kappa = c(0.001, 0.001),
      lambda = c(0.001, 0.001),
      tau_sq_mu = c(1000, 1000),
      tau_sq_Z = c(1000, 1000),
      phi_sq = c(a1, b1, c1),
      ln_prior_c = "reactive",
      options_X = list(sum_to_zero = FALSE, center = FALSE, scale = FALSE),
      align_data = TRUE,
      verbose = TRUE
    )
    
    t1 = med$ln_post_c[,match(c("1,1,0", "1,1,1", "1,0,1", "1,*,1", "0,*,1"),colnames(med$ln_post_c))]
    #t1 = cbind(t1,(1-rowSums(t1)))
    colnames(t1) = c("complete med","partial med","co-local",
                     "partial med (react)","complete med (react)")
    
    
    res1 = rbind(res1, data.frame(beta = sds[i],t(t1[1,])))
    
  }
  
  res1$other.non.med = 1 - (rowSums(exp(res1[,-1])))
  res3$other.non.med = 1 - (rowSums(exp(res3[,-1])))
  res4$other.non.med = 1 - (rowSums(exp(res4[,-1])))
  res5$other.non.med = 1 - (rowSums(exp(res5[,-1])))
  res7$other.non.med = 1 - (rowSums(exp(res7[,-1])))
  res8$other.non.med = 1 - (rowSums(exp(res8[,-1])))
  
  #--ICC
  
  dticc = data.frame(icc = res8$beta,comp_med = res1$complete.med,
                     par_med = res1$partial.med,
                     comp_med_reac = res1$complete.med..react.,
                     par_med_reac = res1$partial.med..react.)
  
  dticc = dticc[order(dticc$icc),]
  dticc[,2:5] = exp(dticc[,2:5])
  dticc$caus = dticc$comp_med + dticc$par_med
  dticc$reac = dticc$comp_med_reac + dticc$par_med_reac
  
  y.smooth.caus <- loess(dticc$caus ~ dticc$icc, span=0.5)$fitted
  y.smooth.reac <- loess(dticc$reac ~ dticc$icc, span=0.5)$fitted
  
  curve1_f <- approxfun(dticc$icc, dticc$caus, rule = 2)
  curve2_f <- approxfun(dticc$icc, dticc$reac, rule = 2)
  
  # Calculate the intersection of curve 1 and curve 2 along the x-axis
  #point_x <- uniroot(function(x) curve1_f(x) - curve2_f(x),
  #                   c(min(dticc$icc), max(dticc$icc)))$root
  difs = y.smooth.caus - y.smooth.reac
  point_x2 = dticc$icc[which(abs(difs) == min(abs(difs)))]
  idx = which(dticc$icc >= point_x2)
  
  if (length(idx) >= 2) {
    cor1 = cor(dticc$icc[idx],dticc$caus[idx])
    cor2 = cor(dticc$icc[idx],dticc$reac[idx])
    
    if (cor1 > 0 & cor2 < 0) {
      icc_thres = point_x2
      
    } else{
      icc_thres = 0
    }
    
  } else {
    icc_thres = 0
  }
  
  iccs = c(iccs,icc_thres)
}



plot(dticc$icc,dticc$caus, col = "blue",
     xlab = "ICC",ylab = "Posterior probability",
     pch=19,main = paste0("Run #",mm),
     ylim = c(0,1))
points(dticc$icc,dticc$reac, col = "red",pch=19)
legend("topright",legend = c("Causal forward","Causal reactive"),
       col = c("blue","red"),pch = 19)


plot(dticc$icc,y.smooth.caus,col="blue",ylim = c(0,1))
points(dticc$icc,y.smooth.reac,col="red")


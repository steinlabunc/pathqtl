
##################################################################################################
# Figure 2A, prepare a Manhattan plot to view causal forward and reactive models (Model 1A and B)

# Manhattan plot for caQTL->eQTL mediation
options(stringAsFactors = F)

library(data.table)
gtf = fread("/proj/steinlab/projects/R00/eQTLanalysis/phenofiles/gene_transcript_v92.csv",data.table = F)
gtf = gtf[which(gtf$type == "gene"),]
idx = which(gtf$strand == "-")
end = gtf$start[idx]
gtf$start[idx] = gtf$end[idx]
gtf$end[idx] = end


pro = read.csv("/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/bmediate/data/progenitor_bmediatR_results_final.csv")
pro = pro[,-match("comb",colnames(pro))]
pro$type = "progenitor"
neuro = read.csv("/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/bmediate/data/neuron_bmediatR_results_final.csv")
neuro$type = "neuron"

dat = rbind(pro,neuro)
dat$tss = gtf$start[match(dat$gene,gtf$gene_id)]
dat$direction = "forward"

pro.reac = read.csv("/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/bmediate/data/progenitor_bmediatR_results_reactive_full_list_final.csv")
# Assign peak chromosome and start site to align with forward data
pro.reac$chr = pro.reac$chr_peak
pro.reac$tss = as.numeric(sapply(pro.reac$peak, 
                                 function(x) unlist(strsplit(x,"_",fixed="TRUE"))[2]))
pro.reac$type = "progenitor"
pro.reac$direction = "reactive"

# Get the common columns and combine two directions
com_columns = intersect(colnames(dat),colnames(pro.reac))
pro.reac = pro.reac[,match(com_columns,colnames(pro.reac))]
dat = dat[,match(com_columns,colnames(dat))]

dat = rbind(dat,pro.reac)


dat$color = "cyan"
dat$pch = 16
dat$pch[which(as.numeric(dat$complete.med) + as.numeric(dat$partial.med) > 0.5)] = 17
dat$pch[which(as.numeric(dat$complete.med..react.) + as.numeric(dat$partial.med..react.) > 0.5)] = 25
dat$color[which(dat$type == "progenitor" & dat$pch == 17)] = "purple"
dat$color[which(dat$type == "progenitor" & dat$pch == 25)] = "purple"
dat$color[which(dat$type == "neuron" & dat$pch == 17)] = "green"
dat$symbol = gtf$gene_name[match(dat$gene,gtf$gene_id)]

dt = data.frame()
chr.lengths = read.csv("/proj/steinlab/projects/R00/eQTLanalysis/chr_length_hg38.csv",header=F)
for (i in 2:22) {
  sub = dat[which(dat$chr== i),]
  sub$tss = as.numeric(sub$tss) + sum(chr.lengths[1:i-1,2])
  dt = rbind(dt,sub)
  
}

dt = rbind(dt,dat[which(dat$chr == "1"),])
dt$tss = as.numeric(dt$tss)
chr1 = dt[which(dt$chr == "1"),]

# Find max-points

mid = c()
for (i in 1:22) {
  sub = dt[which(dt$chr == i),]
  mid = c(mid, max(sub$tss))
}

library(reshape)
library(stringi)
library(scales)

dt$caus = dt$complete.med + dt$partial.med
dt$reac = dt$complete.med..react. + dt$partial.med..react.

dt$caus[which(dt$direction == "reactive")] = -1*dt$reac[which(dt$direction == "reactive")] 

plot(dt$tss,dt$caus,col=alpha(dt$color,0.5),pch=dt$pch,type="p",
     xlab = "Chromosome",
     ylab = "",
     main = "",
     xlim=c(min(dt$tss),max(dt$tss)),ylim=c(-1,1),xaxt="none",frame.plot = FALSE)

axis(1, at = mid, labels = 1:22, cex.axis=1.0,col.axis="black", las=1)
abline(h = c(0.5,0,-0.5),col="red",lty="dashed",lwd=1.3)
abline(v = mid,col="grey",lty="dashed",lwd=1.3)

label = which(dt$pch != 16)
text(dt$tss[label],dt$caus[label],
     labels=dt$symbol[label],
     col=dt$col[label],cex=0.6)


##################################################################################################
# Figure 2B
##################################################################################################

options(stringAsFactors = F)
library(bmediatR)
library(data.table)


setwd("/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/bmediate/scripts/")

gtf = fread("/proj/steinlab/projects/R00/eQTLanalysis/phenofiles/gene_transcript_v92.csv",data.table = F)
gtf = gtf[which(gtf$type == "gene"),]
idx = which(gtf$strand == "-")
end = gtf$start[idx]
gtf$start[idx] = gtf$end[idx]
gtf$end[idx] = end

##################################################################################

temp.link = "/pine/scr/n/i/nil1/tmp/"
pheno.link = "/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/phenotype/"
geno.link = "/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/genotype/eQTL/truncated/"
type = "progenitor"

if (type == "progenitor") {
  L = "P"
  N = 75
  df1 = 55
  df2 = 53
  num = 8
  cov.eqtl = read.table("../../covariates/progenitor.covariates.eqtl.txt")
  cov.caqtl = read.table("../../covariates/progenitor.covariates.caqtl.txt")
} else {
  L = "N"
  N = 57
  df1 = 37
  df2 = 32
  num = 7
  proatac = neuroatac
  prorna = neurorna
  cov.eqtl = read.table("../../covariates/neuron.covariates.eqtl.txt")
  cov.caqtl = read.table("../../covariates/neuron.covariates.caqtl.txt")
}

gene = "ENSG00000134121"
tss = gtf$start[which(gtf$gene_id == gene)]
chr = 3
SNP1 = "chr3:74815:G:A"
center_peak ="chr3_74521_75730"
trun = read.table("/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/genotype/caQTL/truncated/chr3_chromatin_truncated.txt",header=T)
trun$bp1 = as.numeric(sapply(trun$peak, function(x) unlist(strsplit(x,"_",fixed="TRUE"))[2]))
trun$bp2 = as.numeric(sapply(trun$peak, function(x) unlist(strsplit(x,"_",fixed="TRUE"))[3]))
trun$mid = (trun$bp1 + trun$bp2)/2

s1 = tss - 1e+6
s2 = tss + 1e+6

peaks = trun$peak[which(trun$mid >= s1 & trun$mid <= s2)]
dt = data.frame()

for (pp in peaks) {
  SNP = gsub(":",".",SNP1)
  pheno = paste0(pheno.link,"/eQTL/",type,"/chr",chr,"/",L,"-chr",chr,"-",gene,".txt")
  raw = read.table(paste0("../../genotype/caQTL/",type,"/chr",chr,"/",center_peak,".raw"),header=T)

  donors.id = raw$FID
  raw = raw[,grep(SNP,colnames(raw))]
  raw = raw[,-grep("HET",colnames(raw))]
  if (!is.null(ncol(raw))) {
    raw = raw[,1]
  }
  raw[which(raw == 0)] = -1
  raw[which(raw == 1)] = 0
  raw[which(raw == 2)] = 1

  peak = read.table(paste0(pheno.link,"/caQTL/",type,"/chr",chr,"/",pp,".txt"))
  X = as.matrix(raw)
  rownames(X) = donors.id
  colnames(X) = "SNP"

  M1 = peak
  M1 = M1[match(donors.id,M1$V1),]

  M = cbind(M1=M1$V3,M2=M1$V3) # put second M1 just to create the desired format
  rownames(M) = donors.id

  y = read.table(pheno)
  y = y[match(donors.id,y$V1),]

  y = y$V3
  names(y) = donors.id

  cov.eqtl = cov.eqtl[match(donors.id,cov.eqtl$V1),]
  Z_y = as.matrix(cov.eqtl[,-c(1:3)])
  rownames(Z_y) = donors.id
  cov.caqtl = cov.caqtl[match(donors.id,cov.caqtl$V1),]
  Z_M = as.matrix(cov.caqtl[,-c(1:3)])
  rownames(Z_M) = donors.id
  y_res = resid(lm(y ~ as.matrix(Z_y)))
  m_res = resid(lm(M[,1] ~ as.matrix(Z_M)))
  med = bmediatR(
    y = y,
    M = M,
    X = X,
    Z = NULL,
    Z_y = Z_y,
    Z_M = Z_M,
    w = NULL,
    w_y = NULL,
    w_M = NULL,
    kappa = c(0.001, 0.001),
    lambda = c(0.001, 0.001),
    tau_sq_mu = c(1000, 1000),
    tau_sq_Z = c(1000, 1000),
    phi_sq = c(1, 1, 1),
    ln_prior_c = "complete",
    options_X = list(sum_to_zero = FALSE, center = FALSE, scale = FALSE),
    align_data = TRUE,
    verbose = TRUE
  )

  t1 = med$ln_post_c[,match(c("1,1,0", "1,1,1", "1,0,1", "1,*,1", "0,*,1"),colnames(med$ln_post_c))]
  colnames(t1) = c("complete med","partial med","co-local",
                   "partial med (react)","complete med (react)")

  ss = summary(lm(M[,1] ~ X + Z_M))
  caP = ss$coefficients[2,4]
  dt = rbind(dt, data.frame(snp=SNP1,gene=gene, peak_pval  = caP,
                            mediator = pp,t(t1[1,])))


}

dt$complete.med = exp(dt$complete.med)
dt$partial.med = exp(dt$partial.med)
dt$partial.med..react. = exp(dt$partial.med..react.)
dt$complete.med..react. = exp(dt$complete.med..react.)
dt$co.local = exp(dt$co.local)
dt$caus = dt$complete.med + dt$partial.med
dt$reac = dt$complete.med..react. + dt$partial.med..react.
dt$bp1 = as.numeric(sapply(dt$mediator, function(x) unlist(strsplit(x,"_",fixed="TRUE"))[2]))
dt$bp2 = as.numeric(sapply(dt$mediator, function(x) unlist(strsplit(x,"_",fixed="TRUE"))[3]))
dt$mid = (dt$bp1 + dt$bp2)/2


min = min(dt$mid)
max = max(dt$mid)

par(mar = c(5, 4, 4, 4) + 0.3)

plot(dt$mid,
     -log10(dt$peak_pval),
     ylim = c(0,max(-log10(dt$peak_pval))),
     col = "lightgreen",
     ylab = "-log10(p-value)",
     type="l",
     xlab = "Genomic position",
     cex = 0.8,
     xlim = c(min,max))

par(new = TRUE)                             # Add new plot
plot(dt$mid,dt$caus, col = "blue",
     pch=2,
     ylab = "",
     xlab = "",
     axes = FALSE,
     ylim = c(0,1),
     xlim = c(min,max))
points(dt$mid,dt$co.local, col = "black",
       pch=4)
points(dt$mid,dt$reac, col = "red",
       pch=6)


axis(side = 4, at = pretty(range(c(0,1))))      # Add second axis
mtext("Posteior probability", side = 4, line = 3)             # Add second axis label
abline(h = 0.5,col = "grey",lty="dashed")

# gene model

genemart = biomaRt::useMart(biomart="ENSEMBL_MART_ENSEMBL",dataset="hsapiens_gene_ensembl")
genehg38 = Gviz::BiomartGeneRegionTrack(genome="hg38", biomart=genemart, chromosome=8,
                                        start=min, end= max, sshowId=TRUE,
                                        geneSymbols=TRUE,transcriptAnnotation="symbol",
                                        collapseTranscripts="meta",name="ENSEMBL_hg38")

gtrack = Gviz::GenomeAxisTrack();
data <- read.table("/proj/steinlab/projects/R00/eQTLanalysis/Diseasecoloc/cytoBandIdeo.txt.gz",
                   header = F, sep = "\t")
colnames(data) <- c("chrom", "chromStart", "chromEnd", "name", "gieStain")
itrack = Gviz::IdeogramTrack(genome = "hg38", chromosome = 8, bands=data)
Gviz::plotTracks(c(itrack,gtrack,genehg38),
                 main="",transcriptAnnotation="symbol",add53=TRUE,showBandID=TRUE,
                 cex.bands=0.7,stackHeight=0.8,background.title = "white",col.axis="black",
                 col.title="black", cex=0.75,
                 cex.main = 0.9,cex.title=1.5,cex.axis=1.5,just.group="below",
                 collapseTranscripts="meta",
                 from = min,to = max, fontsize = 6,
                 groupAnnotation = "group",just.group = "right")

##################################################################################################
# Figure 2C
##################################################################################################

temp.link = "/pine/scr/n/i/nil1/tmp/"
pheno.link = "/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/phenotype/"
geno.link = "/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/genotype/eQTL/truncated/"
type = "neuron"

if (type == "progenitor") {
  L = "P"
  N = 75
  df1 = 55
  df2 = 53
  num = 8
  cov.eqtl = read.table("../../covariates/progenitor.covariates.eqtl.txt")
  cov.caqtl = read.table("../../covariates/progenitor.covariates.caqtl.txt")
} else {
  L = "N"
  N = 57
  df1 = 37
  df2 = 32
  num = 7
  proatac = neuroatac
  prorna = neurorna
  cov.eqtl = read.table("../../covariates/neuron.covariates.eqtl.txt")
  cov.caqtl = read.table("../../covariates/neuron.covariates.caqtl.txt")
}

gene = "ENSG00000184492"
tss = gtf$start[which(gtf$gene_id == gene)]
chr = 2
SNP1 = "chr2:113503492:T:C"
center_peak ="chr2_113503031_113503850"
trun = read.table("/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/genotype/caQTL/truncated/chr2_chromatin_truncated.txt",header=T)
trun$bp1 = as.numeric(sapply(trun$peak, function(x) unlist(strsplit(x,"_",fixed="TRUE"))[2]))
trun$bp2 = as.numeric(sapply(trun$peak, function(x) unlist(strsplit(x,"_",fixed="TRUE"))[3]))
trun$mid = (trun$bp1 + trun$bp2)/2

s1 = tss - 1e+6
s2 = tss + 1e+6

peaks = trun$peak[which(trun$mid >= s1 & trun$mid <= s2)]
dt = data.frame()

for (pp in peaks) {
  SNP = gsub(":",".",SNP1)
  pheno = paste0(pheno.link,"/eQTL/",type,"/chr",chr,"/",L,"-chr",chr,"-",gene,".txt")
  raw = read.table(paste0("../../genotype/caQTL/",type,"/chr",chr,"/",center_peak,".raw"),header=T)

  donors.id = raw$FID
  raw = raw[,grep(SNP,colnames(raw))]
  raw = raw[,-grep("HET",colnames(raw))]
  if (!is.null(ncol(raw))) {
    raw = raw[,1]
  }
  raw[which(raw == 0)] = -1
  raw[which(raw == 1)] = 0
  raw[which(raw == 2)] = 1

  peak = read.table(paste0(pheno.link,"/caQTL/",type,"/chr",chr,"/",pp,".txt"))
  X = as.matrix(raw)
  rownames(X) = donors.id
  colnames(X) = "SNP"

  M1 = peak
  M1 = M1[match(donors.id,M1$V1),]

  M = cbind(M1=M1$V3,M2=M1$V3) # put second M1 just to create the desired format
  rownames(M) = donors.id

  y = read.table(pheno)
  y = y[match(donors.id,y$V1),]

  y = y$V3
  names(y) = donors.id

  cov.eqtl = cov.eqtl[match(donors.id,cov.eqtl$V1),]
  Z_y = as.matrix(cov.eqtl[,-c(1:3)])
  rownames(Z_y) = donors.id
  cov.caqtl = cov.caqtl[match(donors.id,cov.caqtl$V1),]
  Z_M = as.matrix(cov.caqtl[,-c(1:3)])
  rownames(Z_M) = donors.id
  y_res = resid(lm(y ~ as.matrix(Z_y)))
  m_res = resid(lm(M[,1] ~ as.matrix(Z_M)))
  med = bmediatR(
    y = y,
    M = M,
    X = X,
    Z = NULL,
    Z_y = Z_y,
    Z_M = Z_M,
    w = NULL,
    w_y = NULL,
    w_M = NULL,
    kappa = c(0.001, 0.001),
    lambda = c(0.001, 0.001),
    tau_sq_mu = c(1000, 1000),
    tau_sq_Z = c(1000, 1000),
    phi_sq = c(1, 1, 1),
    ln_prior_c = "complete",
    options_X = list(sum_to_zero = FALSE, center = FALSE, scale = FALSE),
    align_data = TRUE,
    verbose = TRUE
  )

  t1 = med$ln_post_c[,match(c("1,1,0", "1,1,1", "1,0,1", "1,*,1", "0,*,1"),colnames(med$ln_post_c))]
  colnames(t1) = c("complete med","partial med","co-local",
                   "partial med (react)","complete med (react)")

  ss = summary(lm(M[,1] ~ X + Z_M))
  caP = ss$coefficients[2,4]
  dt = rbind(dt, data.frame(snp=SNP1,gene=gene, peak_pval  = caP,
                            mediator = pp,t(t1[1,])))


}

dt$complete.med = exp(dt$complete.med)
dt$partial.med = exp(dt$partial.med)
dt$partial.med..react. = exp(dt$partial.med..react.)
dt$complete.med..react. = exp(dt$complete.med..react.)
dt$co.local = exp(dt$co.local)
dt$caus = dt$complete.med + dt$partial.med
dt$reac = dt$complete.med..react. + dt$partial.med..react.
dt$bp1 = as.numeric(sapply(dt$mediator, function(x) unlist(strsplit(x,"_",fixed="TRUE"))[2]))
dt$bp2 = as.numeric(sapply(dt$mediator, function(x) unlist(strsplit(x,"_",fixed="TRUE"))[3]))
dt$mid = (dt$bp1 + dt$bp2)/2


min = min(dt$mid)
max = max(dt$mid)

par(mar = c(5, 4, 4, 4) + 0.3)

plot(dt$mid,
     -log10(dt$peak_pval),
     ylim = c(0,max(-log10(dt$peak_pval))),
     col = "lightgreen",
     ylab = "-log10(p-value)",
     type="l",
     xlab = "Genomic position",
     cex = 0.8,
     xlim = c(min,max))

par(new = TRUE)                             # Add new plot
plot(dt$mid,dt$caus, col = "blue",
     pch=2,
     ylab = "",
     xlab = "",
     axes = FALSE,
     ylim = c(0,1),
     xlim = c(min,max))
points(dt$mid,dt$co.local, col = "black",
       pch=4)
points(dt$mid,dt$reac, col = "red",
       pch=6)


axis(side = 4, at = pretty(range(c(0,1))))      # Add second axis
mtext("Posteior probability", side = 4, line = 3)             # Add second axis label
abline(h = 0.5,col = "grey",lty="dashed")

# gene model

genemart = biomaRt::useMart(biomart="ENSEMBL_MART_ENSEMBL",dataset="hsapiens_gene_ensembl")
genehg38 = Gviz::BiomartGeneRegionTrack(genome="hg38", biomart=genemart, chromosome=8,
                                        start=min, end= max, sshowId=TRUE,
                                        geneSymbols=TRUE,transcriptAnnotation="symbol",
                                        collapseTranscripts="meta",name="ENSEMBL_hg38")

gtrack = Gviz::GenomeAxisTrack();
data <- read.table("/proj/steinlab/projects/R00/eQTLanalysis/Diseasecoloc/cytoBandIdeo.txt.gz",
                   header = F, sep = "\t")
colnames(data) <- c("chrom", "chromStart", "chromEnd", "name", "gieStain")
itrack = Gviz::IdeogramTrack(genome = "hg38", chromosome = 2, bands=data)
Gviz::plotTracks(c(itrack,gtrack,genehg38),
                 main="",transcriptAnnotation="symbol",add53=TRUE,showBandID=TRUE,
                 cex.bands=0.7,stackHeight=0.8,background.title = "white",col.axis="black",
                 col.title="black", cex=0.75,
                 cex.main = 0.9,cex.title=1.5,cex.axis=1.5,just.group="below",
                 collapseTranscripts="meta",
                 from = min,to = max, fontsize = 6,
                 groupAnnotation = "group",just.group = "right")


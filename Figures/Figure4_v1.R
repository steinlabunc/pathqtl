
#######################################################################
####### Create Figure 4 to evaluate trans-eQTL
#######################################################################

options(stringAsFactors = F)
library(bmediatR)
library(data.table)

link = "/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/trans-eQTL/"


library(data.table)
gtf = fread("/proj/steinlab/projects/R00/eQTLanalysis/phenofiles/gene_transcript_v92.csv",data.table = F)
gtf = gtf[which(gtf$type == "gene"),]
idx = which(gtf$strand == "-")
end = gtf$start[idx]
gtf$start[idx] = gtf$end[idx]
gtf$end[idx] = end

# Inputs

pro = data.frame()
files = list.files(path = link, 
                   pattern = glob2rx("trans.progenitor.big.bmediate.chr*_updated_w_complete.csv"),
                   full.names = T)
for (f in files) {
  dt = read.csv(f)
  pro = rbind(pro,dt)
}

pro$complete.med = exp(pro$complete.med)
pro$co.local = exp(pro$co.local)
pro$partial.med = exp(pro$partial.med)

neuro = data.frame()
files = list.files(path = link, 
                   pattern = glob2rx("trans.neuron.big.bmediate.chr*_updated_w_complete.csv"),
                   full.names = T)
for (f in files) {
  dt = read.csv(f)
  neuro = rbind(neuro,dt)
}

neuro$complete.med = exp(neuro$complete.med)
neuro$co.local = exp(neuro$co.local)
neuro$partial.med = exp(neuro$partial.med)
 

# eliminate FP QTLs

cross_map2 = fread("/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/trans-eQTL/data/hg38_cross_mappability_strength_symmetric_mean_sorted.txt.gz",data.table=F)
cross_map2$id = paste0(cross_map2$V1,"_",cross_map2$V2)

pro$id1 = paste0(pro$cis_gene,"_",pro$trans_gene)
pro$id2 = paste0(pro$trans_gene,"_",pro$cis_gene)
pro$cross = cross_map2$V3[match(pro$id1,cross_map2$id)]
pro$cross = cross_map2$V3[match(pro$id2,cross_map2$id)]
pro$cross = log2(pro$cross)
pro1 = pro[is.na(pro$cross),]
pro2 = pro[which(pro$cross < 5),]
pro = rbind(pro1,pro2)

neuro$id1 = paste0(neuro$cis_gene,"_",neuro$trans_gene)
neuro$id2 = paste0(neuro$trans_gene,"_",neuro$cis_gene)
neuro$cross = cross_map2$V3[match(neuro$id1,cross_map2$id)]
neuro$cross = cross_map2$V3[match(neuro$id2,cross_map2$id)]
neuro$cross = log2(neuro$cross)
neuro1 = neuro[is.na(neuro$cross),]
neuro2 = neuro[which(neuro$cross < 5),]
neuro = rbind(neuro1,neuro2)

elim.pro = data.frame()
for (i in c(12,13,19)) {
  ss = read.csv(paste0(link,"progenitor_eliminated_cisQTLs_chr",i,".csv"))
  elim.pro = rbind(elim.pro,ss)
}
pro$comb = paste0(pro$snp,"_",pro$cis_gene)
pro = pro[which(pro$comb %in% setdiff(pro$comb,elim.pro$x)),]

elim.neuro = read.csv(paste0(link,"neuron_eliminated_cisQTLs_chr21.csv"))
neuro$comb = paste0(neuro$snp,"_",neuro$cis_gene)
neuro = neuro[which(neuro$comb %in% setdiff(neuro$comb,elim.neuro$x)),]

pro.caus = pro[which(pro$complete.med + pro$partial.med > 0.5),]
neuro.caus = neuro[which(neuro$complete.med + neuro$partial.med > 0.5),]

print(length(unique(pro.caus$cis_gene)))
print(length(unique(pro.caus$trans_gene)))
print(length(unique(pro.caus$snp)))
print(length(unique(neuro.caus$cis_gene)))
print(length(unique(neuro.caus$trans_gene)))
print(length(unique(neuro.caus$snp)))

###########################################################################
# Figure 4A
###########################################################################

pro$cis_biotype = gtf$gene_biotype[match(pro$cis_gene,gtf$gene_id)]
pro$trans_biotype = gtf$gene_biotype[match(pro$trans_gene,gtf$gene_id)]
neuro$cis_biotype = gtf$gene_biotype[match(neuro$cis_gene,gtf$gene_id)]
neuro$trans_biotype = gtf$gene_biotype[match(neuro$trans_gene,gtf$gene_id)]

pro$cis_symbol = gtf$gene_name[match(pro$cis_gene,gtf$gene_id)]
pro$trans_symbol = gtf$gene_name[match(pro$trans_gene,gtf$gene_id)]
neuro$cis_symbol = gtf$gene_name[match(neuro$cis_gene,gtf$gene_id)]
neuro$trans_symbol = gtf$gene_name[match(neuro$trans_gene,gtf$gene_id)]

subp = pro[which(pro$cis_biotype %in% c("protein_coding","lincRNA","antisense") &
                   pro$trans_biotype %in% c("protein_coding","lincRNA","antisense")),]
subp = subp[match(unique(subp$id1),subp$id1),] # unique cis-trans pairs
subp$caus = subp$complete.med + subp$partial.med
dtp = data.frame(table(subp$trans_gene))
dtp = dtp[order(-as.numeric(dtp$Freq)),]
genes = dtp$Var1[1:20]
dat_pro = data.frame(subp$cis_symbol[which(subp$trans_gene %in% genes)],
                     subp$trans_symbol[which(subp$trans_gene %in% genes)])
#subp = subp[match(unique(subp$trans_gene),subp$trans_gene),] # unique trans genes

subn = neuro[which(neuro$cis_biotype %in% c("protein_coding","lincRNA","antisense") &
                    neuro$trans_biotype %in% c("protein_coding","lincRNA","antisense")),]
subn = subn[match(unique(subn$id1),subn$id1),] # unique cis-trans pairs
subn$caus = subn$complete.med + subn$partial.med
dtn = data.frame(table(subn$trans_gene))
dtn = dtn[order(-as.numeric(dtn$Freq)),]
genes = dtn$Var1[1:20]
dat_neuro = data.frame(subn$cis_symbol[which(subn$trans_gene %in% genes)],
                     subn$trans_symbol[which(subn$trans_gene %in% genes)])
#subn = subn[match(unique(subn$trans_gene),subn$trans_gene),] # unique trans genes


suppressPackageStartupMessages(library(circlize))

chordDiagram(dat_pro, transparency = 0.5)
chordDiagram(dat_neuro, transparency = 0.5)

###########################################################################
# Figure 4B
###########################################################################


dtp = data.frame(table(subp$cis_gene))
subp$frq = dtp$Freq[match(subp$cis_gene,dtp$Var1)]
dtn = data.frame(table(subn$cis_gene))
subn$frq = dtn$Freq[match(subn$cis_gene,dtn$Var1)]

summary(lm(subp$frq~subp$cross))
par(mfrow = c(2,1))

subp$col = "purple"
subp$cross_form = subp$cross
subp$cross_form[is.na(subp$cross)] = 0
subp$col[is.na(subp$cross)] = "black"

plot(subp$frq,
     subp$cross_form,
     col = subp$col, pch = 19,
     ylab = "Cross-mapp", cex = 0.5,
     xlab = "Number of trans-eGenes mediated")

subn$col = "green"
subn$cross_form = subn$cross
subn$cross_form[is.na(subn$cross)] = 0
subn$col[is.na(subn$cross)] = "black"

plot(subn$frq,
     subn$cross_form,
     col = subn$col, pch = 19,
     ylab = "Cross-mapp", cex = 0.5,
     xlab = "Number of trans-eGenes mediated")


###########################################################################
# Figure 4C
###########################################################################

###########################################################################
# Figure 4D
###########################################################################
pheno.link = "/proj/steinlab/projects/R00/eQTLanalysis/phenofiles/vstNorm/"
traw = read.table(paste0("/proj/steinlab/projects/R00/eQTLanalysis/genofiles/allele_order_corrected_genofiles/chr14_P.significant.traw"),header=T)

pheno = read.table(paste0(pheno.link,"P-chr19-ENSG00000105576.txt"))
pheno$dn = paste0(pheno$V1,"_",pheno$V2)

raw = traw[which(traw$SNP == "chr14:104944080:G:A"),]
donors.id = paste0(pheno$V1,"_",pheno$V2)
raw = raw[,match(donors.id,colnames(raw))]

A1 = "G"
A2 = "A"

df <- data.frame(samples=pheno$V1, genotype = as.factor(raw),
                 expression= as.numeric(pheno$V3));
df$genotype <- factor(df$genotype, levels=c(2, 1, 0))

cbPalette <- c("#999999", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7");

p1=ggplot(df, aes(genotype, expression)) +
  geom_jitter(colour = "purple",alpha = 0.7, size=3,width = 0.12) +
  scale_x_discrete(labels = c(paste0(A2,"/",A2),paste0(A2,"/",A1),paste0(A1,"/",A1))) +
  geom_boxplot(alpha = 0.3, fill = "white",lwd=1.5) +
  theme_bw(base_size = 15) +
  #geom_smooth(method = 'lm',col="purple", aes(group=1), se=FALSE) +
  theme(axis.text=element_text(size=15),
        axis.title=element_text(size=15))


###########################################################################
# Figure 4E
###########################################################################

###########################################################################
# Figure 4F
###########################################################################
options(stringAsFactors = F)
library(TFBSTools)
library(JASPAR2020)

# E2F3
pfm <- getMatrixByID(JASPAR2020, ID="MA0469.1")
pwm1 <- toPWM(pfm)

pfm <- getMatrixByID(JASPAR2020, ID="MA0469.2")
pwm2 <- toPWM(pfm)

pfm <- getMatrixByID(JASPAR2020, ID="MA0469.3")
pwm3 <- toPWM(pfm)

# E2F4
pfm <- getMatrixByID(JASPAR2020, ID="MA0470.1")
pwm4 <- toPWM(pfm)

pfm <- getMatrixByID(JASPAR2020, ID="MA0470.2")
pwm5 <- toPWM(pfm)

pwmList <- PWMatrixList(pwm3,pwm5,
                         use.names=TRUE)


library(Biostrings)
library(BSgenome.Hsapiens.UCSC.hg38)


tnpo2 <- DNAString(as.character(Biostrings::getSeq(BSgenome.Hsapiens.UCSC.hg38, paste0("chr",19),
                                                   12723511,12724511)))

sitesetList <- searchSeq(pwmList, tnpo2, seqname="seq1",
                         min.score="80%", strand="+")

head(writeGFF3(sitesetList))
as(sitesetList, "data.frame")
dt = as(sitesetList, "data.frame")

pdf("/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/tfbs_e2f3_4.pdf",
    width = 7,height = 5)
plot(c(0, 1000), c(0, 100), type = "n", xlab = "", ylab = "")

for (i in 1:nrow(dt)) {
  rect(dt$start[i], dt$relScore[i]*100, dt$end[i], dt$relScore[i]*100+5,col = "orange",border = TRUE)
}


cdk5 <- DNAString(as.character(Biostrings::getSeq(BSgenome.Hsapiens.UCSC.hg38, paste0("chr",7),
                                                  151057397, 151058397)))

sitesetList <- searchSeq(pwmList, cdk5, seqname="seq1",
                         min.score="80%", strand="-")

head(writeGFF3(sitesetList))
as(sitesetList, "data.frame")

dt = as(sitesetList, "data.frame")
for (i in 1:nrow(dt)) {
  rect(dt$start[i], dt$relScore[i]*100, dt$end[i], dt$relScore[i]*100+5,col = "royalblue1",border = TRUE)
}

abline(v = 500,col = "red",lty="dashed")
legend("bottomright",legend = c("TNPO2","CDK50"),
       col = c("orange","royalblue1"),
       lty= 1)

dev.off()


###########################################################################
# Figure 4G
###########################################################################

data = gtf[which(gtf$seqnames %in% 1:22),]
e2f = read.csv("/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/e2f_motif_scores_genomewide_80_match.csv")
e2f$gene = data$gene_id
e2f$CDCA4 = "NO"

CDCA4_trans = unique(subp$trans_gene[which(subp$cis_symbol == "CDCA4")])
idx = match(CDCA4_trans,e2f$gene)
e2f$CDCA4[idx] = "YES"

count_nonzero <- function(x) {
  return(length(x[which(x != 0)]))
}

e2f$count = apply(as.matrix(e2f[,1:4]),1,count_nonzero)
e2f$biotype = gtf$gene_biotype[match(e2f$gene,gtf$gene_id)]

ss = e2f[which(e2f$biotype == "protein_coding"),]

t = t.test(ss$count ~ ss$CDCA4,paired=F)

boxplot(ss$count ~ ss$CDCA4)


#########################################################################################

e2f = read.csv("/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/e2f_motif_scores_genomewide_80_match_dat.csv")
eqtl = read.csv(paste0("/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/bmediate/data/",type,"_bmediatR_results_causal_final_trans.csv"))

gr1 = GRanges(c(e2f$gene),IRanges(c(e2f$start),c(e2f$end)))
tt = reduce(gr1)
dat = data.frame(tt)
tab = data.frame(table(dat$seqnames))

gene_dat = data.frame(gene = gtf$gene_id,n = 0)
gene_dat$n = tab$Freq[match(gene_dat$gene,tab$Var1)]
gene_dat$n[is.na(gene_dat$n)] = 0
trans_genes = unique(eqtl$trans_gene[which(eqtl$cis_gene == "ENSG00000170779")])
gene_dat$CDCA4 = "NO"
gene_dat$CDCA4[match(trans_genes,gene_dat$gene)] = "YES"
gene_dat$chr = gtf$seqnames[match(gene_dat$gene,gtf$gene_id)]
gene_dat = gene_dat[which(gene_dat$chr %in% 1:22),]

t.test(gene_dat$n ~ gene_dat$CDCA4,paired=F)
boxplot(gene_dat$n ~ gene_dat$CDCA4)

gene_dat$biotype = gtf$gene_biotype[match(gene_dat$gene,gtf$gene_id)]
sub_gene_dat = gene_dat[which(gene_dat$biotype == "protein_coding"),]
t.test(sub_gene_dat$n ~ sub_gene_dat$CDCA4,paired=F)
boxplot(sub_gene_dat$n ~ sub_gene_dat$CDCA4)



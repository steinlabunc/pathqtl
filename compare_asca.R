# Assign ASCA findings to compare Causal vs independence

#----Functions

# Make LD

makeLD <- function(snp,k){
  cat("module add plink \n")
  system(paste0("module add plink \n plink --bfile ../../../atac-qtl/EMMAXResult/Genotype_Hg38/",k,
                ".dose.R2g03.QC.IDfixed --keep ../../LD/donorlist_ld.txt --ld-snp ",snp,
                " --ld-window 200000 --ld-window-kb 2500 --ld-window-r2 0.2 --out /pine/scr/n/i/nil1/eQTL/",snp," --r2"))
  if (file.exists(paste0("/pine/scr/n/i/nil1/eQTL/",snp,".ld")) == T) {
    ld = read.table(paste0("/pine/scr/n/i/nil1/eQTL/",snp,".ld"), header = T)
    return(ld)
  }
}

combAll = function(vec) {
return(length(unlist(strsplit(vec,""))))
}


#------------------------------------------------------------------#

asca = read.csv("/proj/steinlab/projects/R00/atac-qtl/AlleleSpeCaQTLs/ASEResults/Progenitor_allele_specific_caQTLs.csv")
asca$chr = gsub("chr","",asca$Chr)
asca$all = apply(asca[,c("refAllele","altAllele")],1,combAll)
asca$bp = sapply(asca$variantID, function(x) unlist(strsplit(x,":",fixed="TRUE"))[2])
asca$comb = paste0(asca$chr,"_",asca$bp,"_",asca$all)
asca.sig = asca[which(as.numeric(asca$padj) < 0.05),]
asca.ns= asca[which(as.numeric(asca$padj) >= 0.05),]
pro = read.csv("/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/bmediate/data/progenitor_bmediatR_results_final.csv",header=T)
pro = pro[which(pro$icc_gene > 0 & pro$icc_peak > 0),]

snps = unique(pro$snp)

dat = data.frame()
for (snp in snps) {
  k = sapply(snp, function(x) unlist(strsplit(x,":",fixed="TRUE"))[1])
  k = gsub("chr","",k)
  ld = makeLD(snp,k)
  ld = ld[which(ld$R2 > 0.8),]
  # SNPs supported by ASCA
  ld$A1 = sapply(ld$SNP_B, function(x) unlist(strsplit(x,":",fixed="TRUE"))[3])
  ld$A2 = sapply(ld$SNP_B, function(x) unlist(strsplit(x,":",fixed="TRUE"))[4])
  ld$all = apply(ld[,c("A1","A2")],1,combAll)
  ld$comb = paste0(ld$CHR_A,"_",ld$BP_B,"_",ld$all)
  comsnps = intersect(ld$comb,asca.sig$comb)


  if (length(comsnps) >= 1) {
    minP = min(as.numeric(asca$padj[match(comsnps,asca$comb)]))
    sub = data.frame(snp = snp,asca = paste0(comsnps,collapse = "_"),minP = minP)

  } else if (length(intersect(asca.ns$comb,ld$comb)) >= 1) {
    sub = data.frame(snp = snp,asca = "NO",minP = 1)
  } else if (!length(intersect(asca$comb,ld$comb)) >= 1) {
    sub = data.frame(snp = snp,asca = "Not_tested", minP =1)
  }

  dat  =rbind(dat,sub)

}


write.csv(dat,file = "/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/bmediate/data/asca_overlap_progenitor.csv",
quote= F, col.names=T,row.names=F)






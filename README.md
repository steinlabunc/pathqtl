# README #

### This repository for script of the study titled "Inferring cell-type-specific causal gene regulatory networks during human neurogenesis", DOI: doi: https://doi.org/10.1101/2022.04.25.488920 

* The codes used to generate figures can be found under /Figures folder
* The codes used for each analyses to evaluate each of the models represented Figure 1 can be found under Model 1A, Model 1B and Model2 folders
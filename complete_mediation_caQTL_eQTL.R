options(stringAsFactors = F)
library(bmediatR)
library(data.table)

type = "progenitor"
setwd("/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/bmediate/scripts/")

##################################################################################

temp.link = "/pine/scr/n/i/nil1/tmp/"
pheno.link = "/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/phenotype/"
geno.link = "/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/genotype/eQTL/truncated/"

if (type == "progenitor") {
  L = "P"
  N = 75
  df1 = 55
  df2 = 53
  num = 8
  cov.eqtl = read.table("../../covariates/progenitor.covariates.eqtl.txt")
  cov.caqtl = read.table("../../covariates/progenitor.covariates.caqtl.txt")
} else {
  L = "N"
  N = 57
  df1 = 37
  df2 = 32
  num = 7
  proatac = neuroatac
  prorna = neurorna
  cov.eqtl = read.table("../../covariates/neuron.covariates.eqtl.txt")
  cov.caqtl = read.table("../../covariates/neuron.covariates.caqtl.txt")
}

#-------------------------Functions--------------------------------#

getSE = function(beta,p,n,df){
  beta = as.numeric(beta)
  p = as.numeric(p)
  n = as.numeric(n)
  df = as.numeric(df)
  t_val <- qt(p/2, df = df) # Calculating the t-value using quantile function
  se = abs(beta)/abs(t_val) # Calculating standard error
  return(se)
}

getR = function(maf,se,beta,N) {
  maf = as.numeric(maf)
  se = as.numeric(se)
  beta = as.numeric(beta)
  m = maf*(1-maf)
  r = (2*beta*beta*m) / ((2*beta*beta*m) + (se*se*2*N*m))
  return(r)
}

makeRaw = function(snp,chr) {
  #snp = gsub(".",":",snp)
  system(paste0("module add plink \n","plink --bfile /proj/steinlab/projects/R00/eQTLanalysis/path_QTL/genotype/caQTL/chr",chr,"_",L,".uniq --snp ",snp," --recode AD --out /pine/scr/n/i/nil1/tmp/",snp,"_",type))
  raw = read.table(paste0("/pine/scr/n/i/nil1/tmp/",snp,"_",type,".raw"),header = T)
  return(raw)
}

pro = read.table(paste0("/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/data/",type,"_inpeak_updated.txt"),header=T)
pro$chr = sapply(pro$snp, function(x) unlist(strsplit(x,":",fixed = "TRUE"))[1])
pro$chr = gsub("chr","",pro$chr)


frq = fread(paste0("/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/genotype/eQTL/",type,".snps.frq"),data.table=F)
pro$maf = frq$MAF[match(pro$snp,frq$SNP)]

cat("Computing heritability...\n")

#---Heritability------#

#---M

pro$r2.peak = numeric(nrow(pro))
pro$beta.peak = numeric(nrow(pro))
for (i in 1:nrow(pro)) {
  emmax = read.table(paste0("../../EMMAX/",type,"/caQTL/chr",pro$chr[i],"/",pro$peak[i],".ps"))
  emmax = emmax[which(emmax$V1 == pro$snp[i]),]
  se = getSE(beta = emmax$V2,p = emmax$V3,n=N,df=df1)
  r2 = getR(maf=pro$maf[i],se=se,beta=emmax$V2,N=N)
  pro$r2.peak[i] = r2
  pro$beta.peak[i] = emmax$V2
}

#---y

pro$r2.gene = numeric(nrow(pro))
pro$beta.gene= numeric(nrow(pro))

for (i in 1:nrow(pro)) {
  emmax = read.table(paste0("../../EMMAX/",type,"/eQTL/chr",pro$chr[i],"/",L,"-chr",pro$chr[i],"-",pro$gene[i],".ps"))
  emmax = emmax[which(emmax$V1 == pro$snp[i]),]
  se = getSE(beta = emmax$V2,p = emmax$V3,n=N,df=df2)
  pro$r2.gene[i] = getR(maf=pro$maf[i],se=se,beta=emmax$V2,N=N)
  pro$beta.gene[i] = emmax$V2
}


cov.gene = read.table(paste0("../../covariates/",type,".covariates.eqtl.txt"))
cov.peak = read.table(paste0("../../covariates/",type,".covariates.caqtl.txt"))
cov.peak = cov.peak[match(cov.gene$V1,cov.peak$V1),]

pro$beta.y = numeric(nrow(pro))
pro$pve.y = numeric(nrow(pro))
pro$big.beta.y = numeric(nrow(pro))
pro$big.beta.m = numeric(nrow(pro))
pro$big.pval.y = numeric(nrow(pro))
pro$big.pval.m = numeric(nrow(pro))

for (i in 1:nrow(pro)) {
  gene = read.table(paste0("../../phenotype/eQTL/",type,"/chr",pro$chr[i],"/",L,"-chr",pro$chr[i],"-",pro$gene[i],".txt"))
  peak = read.table(paste0("../../phenotype/caQTL/",type,"/chr",pro$chr[i],"/",pro$peak[i],".txt"))
  peak = peak[match(gene$V1,peak$V1),]
  gene.res = resid(lm(gene$V3 ~ as.matrix(cov.gene[,-c(1:3)])))
  peak.res = resid(lm(peak$V3 ~ as.matrix(cov.peak[,-c(1:3)])))
  lm = lm(gene.res~peak.res)
  pro$pve.y[i] = summary(lm)$r.squared
  pro$beta.y[i] = summary(lm)$coefficient[2,1]
  
  file1 = paste0("/proj/steinlab/projects/R00/eQTLanalysis/Raw_data/cell/",type,"/chr",pro$chr[i],
                 "/",L,"-chr",pro$chr[i],"-",pro$gene[i],".ps.gz")
  if (file.exists(file1)) {
    emmax.gene = read.table(file1,header=F)
    emmax.gene = emmax.gene[which(emmax.gene$V1 == pro$snp[i]),]
    if(nrow(emmax.gene) >= 1) {
      pro$big.beta.y[i] = emmax.gene$V2
      pro$big.pval.y[i] = emmax.gene$V3
    }
  }
  
  type_big = paste(toupper(substr(type, 1, 1)), substr(type, 2, nchar(type)), sep="")
  file2 = paste0("/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/",type_big,"/CSAW/PCAs_",num,"/",type_big,"/RawData/chr",pro$chr[i],
                 "/",pro$peak[i],"_",type_big,".ps.gz")
  if (file.exists(file2)) {
    emmax.peak = read.table(file2,header=F)
    emmax.peak = emmax.peak[which(emmax.peak$V1 == pro$snp[i]),]
    if (nrow(emmax.peak) >= 1) {
      pro$big.beta.m[i] = emmax.peak$V2
      pro$big.pval.m[i] = emmax.peak$V3
    }
  }
  
}

# Add ICCs
icc_peak = data.frame()
for ( i in 1:22) {
  dt = read.table(paste0("/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/bmediate/icc/",type,"_chr",i,"_icc_peak.txt"),header=T)
  icc_peak = rbind(icc_peak,dt)
}

icc_gene = data.frame()
for ( i in 1:22) {
  dt = read.table(paste0("/proj/steinlab/projects/R00/eQTLanalysis/path_QTL/bmediate/icc/",type,"_chr",i,"_icc_gene.txt"),header=T)
  icc_gene = rbind(icc_gene,dt)
}

pro$icc_gene = icc_gene$icc[match(pro$gene,icc_gene$gene)]
pro$icc_peak = icc_peak$icc[match(pro$peak,icc_peak$peak)]

a1 = 1
b1 = 1
c1 = 1


dt = data.frame()

for (j in 1:nrow(pro)) {
  sub = pro[j,]
  k = sub$chr # Learn chromosome
  SNP = gsub(":",".",sub$snp)
  pheno = paste0(pheno.link,"/eQTL/",type,"/chr",k,"/",L,"-chr",k,"-",sub$gene,".txt")
  geno = paste0(geno.link,type,"/chr",k,"/",L,"-chr",k,"-",sub$gene)
  raw = read.table(paste0("../../genotype/caQTL/",type,"/chr",k,"/",sub$peak,".raw"),header=T)
  #raw = makeRaw(snp=sub$snp,chr = k)
  #raw=read.table(paste0("/pine/scr/n/i/nil1/tmp/",sub$snp,"_",type,".raw"),header = T)
  donors.id = raw$FID
  raw = raw[,grep(SNP,colnames(raw))]
  raw = raw[,-grep("HET",colnames(raw))]
  if (!is.null(ncol(raw))) {
    raw = raw[,1]
  }
  raw[which(raw == 0)] = -1
  raw[which(raw == 1)] = 0
  raw[which(raw == 2)] = 1
  
  
  
  peak = read.table(paste0(pheno.link,"/caQTL/",type,"/chr",sub$chr,"/",sub$peak,".txt"))
  X = as.matrix(raw)
  rownames(X) = donors.id
  colnames(X) = "SNP"
  
  M1 = peak
  M1 = M1[match(donors.id,M1$V1),]
  
  M = cbind(M1=M1$V3,M2=M1$V3) # put second M1 just to create the desired format
  rownames(M) = donors.id
  
  y = read.table(pheno)
  y = y[match(donors.id,y$V1),]
  
  y = y$V3
  names(y) = donors.id
  
  cov.eqtl = cov.eqtl[match(donors.id,cov.eqtl$V1),]
  Z_y = as.matrix(cov.eqtl[,-c(1:3)])
  rownames(Z_y) = donors.id
  cov.caqtl = cov.caqtl[match(donors.id,cov.caqtl$V1),]
  Z_M = as.matrix(cov.caqtl[,-c(1:3)])
  rownames(Z_M) = donors.id
  y_res = resid(lm(y ~ as.matrix(Z_y)))
  m_res = resid(lm(M[,1] ~ as.matrix(Z_M)))
  
  
  med = bmediatR(
    y = y,
    M = M,
    X = X,
    Z = NULL,
    Z_y = Z_y,
    Z_M = Z_M,
    w = NULL,
    w_y = NULL,
    w_M = NULL,
    kappa = c(0.001, 0.001),
    lambda = c(0.001, 0.001),
    tau_sq_mu = c(1000, 1000),
    tau_sq_Z = c(1000, 1000),
    phi_sq = c(1, 1, 1),
    ln_prior_c = "complete",
    options_X = list(sum_to_zero = FALSE, center = FALSE, scale = FALSE),
    align_data = TRUE,
    verbose = TRUE
  )
  
  t1 = med$ln_post_c[,match(c("1,1,0", "1,1,1", "1,0,1", "1,*,1", "0,*,1"),colnames(med$ln_post_c))]
  colnames(t1) = c("complete med","partial med","co-local",
                   "partial med (react)","complete med (react)")
  dt = rbind(dt, data.frame(sub, mediator = sub$peak,t(t1[1,])))
  
  
  
}

dt$complete.med = exp(dt$complete.med)
dt$partial.med = exp(dt$partial.med)
dt$partial.med..react. = exp(dt$partial.med..react.)
dt$complete.med..react. = exp(dt$complete.med..react.)

pro.reac = dt[which(dt$partial.med..react. + dt$complete.med..react. > 0.5),]
pro.caus = dt[which(dt$partial.med + dt$complete.med > 0.5),]
pro.caus$symbol = gtf$gene_name[match(pro.caus$gene,gtf$gene_id)]
pro.caus = pro.caus[which(pro.caus$icc_gene > 0 & pro.caus$icc_peak > 0),]

dt$symbol = gtf$gene_name[match(dt$gene,gtf$gene_id)]
dt = dt[which(dt$icc_gene > 0 & dt$icc_peak > 0),]

write.csv(pro.caus,file= paste0("../data/",type,"_bmediatR_results_final.csv"),
          quote=F,col.names=T,row.names=F)

write.csv(dt,file= paste0("../data/",type,"_bmediatR_results_final.csv"),
          quote=F,col.names=T,row.names=F)

